/**/
// Function paramenters are variables that wait for a value from an arguement in the function invocation
function sayMyName(name){
	console.log('My name is ' + name)
}
// Function arguments are values like strings, numbers, etc. that you can pass onto the function you are invoking
sayMyName('Slim Shady')

// Function paramenters are re-usable bt invoking them. Make sure to declare them first
sayMyName('Vice Ganda')
sayMyName('Andrew Wiggins')
/**/

/**/
// You can also pass variables as arguments to function
let myName = 'Miguel'
sayMyName(myName)
/**/



/**/
// You can use arguments and parameters to make the data inside your function dynamic
function sumOfTwoNumbers(firstNumber, secondNumber){
	let sum = 0

	sum = firstNumber + secondNumber

	console.log(sum)
}

sumOfTwoNumbers(10, 15)
sumOfTwoNumbers(99, 1)
sumOfTwoNumbers(50, 20)
/**/
 

/**/
// You can pass a function as an argument for another function. Do not pass that function with parenthesis, only with its function name
function argumentFunction(){
	console.log('This is a function that was passed as an argument')
}

function parameterFunction(argumentFunction){
	argumentFunction()
}

parameterFunction(argumentFunction)

/**/
// Real World Application 
/*
	Imagine a product page where you have a 'add to cart' button where in you have an button element itself displayed on the page but you don't have the functionality of it yet. This is where passing a function as an argument comes in, in order for us to add or have access to a function to add functionality to your button
*/
function addProductToCart(){
	console.log('Click me to add product to cart')
}
let addToCartBtn = document.querySelector('#add-to-cart-btn')

addToCartBtn.addEventListener('click', addProductToCart)
/**/

/**/
// If you add an extra or if you lack an argument, JS won't throw an erroe at you, instead it will ignore the extra argument and turn the lacking parameter into undefined
function displayFullName(firstName, middleInitial, lastName, extraText){
	console.log('Your full name is: ' +  firstName + ' ' + middleInitial + '.' + ' ' + lastName + ' ' + extraText)

}

displayFullName('Miguel', 'C', 'Yulo')
displayFullName('Aisha', 'F', 'Thadhani', 'bb')
/**/


/**/
// SRING INTERPOLATION - instead of regular concratination, we can 'interpolate' or inject the variable within the string itself
/*function displayMovieDetails(title, synopsis, director){
	console.log(`The movie is titled ${title}`)
	console.log(`The synopsis is ${synopsis}`)
	console.log(`The director is ${director}`)
}
displayMovieDetails('Sisterakas', 'I forgot the synopsis', 'Wenn Deramas')*/
/**/
